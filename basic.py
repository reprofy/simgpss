# -*- coding: utf-8 -*-
"""
Created on Sun Nov 23 19:48:04 2014

@author: Obrezkov D.
"""
import simpy
import matplotlib.pyplot as plt


RUNTIME = 14000
FIGURE = 1
"""
There is a message sending when interrupts occur
These messages' format:
[type of message, message body]
supported types are: "status", "transact", "signal"
Messages:
["status", sender, set of types of transacts which could be processed]
["transact", Transact object]
["signal", kind of signal] 
Signals are for locks, kind of signals : "open", "close"
"""


class Transact:
    #dict which contains entries which are passed by this transact and
    #taken time for each
    name = ""
    #object in which this transact is processing
    current_stage = 0
    def __init__(self, name = "Undefined", current_stage = 0, \
    incomplete_req = 0, transact_type = "all"):
        self.name = name
        self.current_stage = current_stage #not using now
        self.incomplete_req = incomplete_req #not using now
        #path has the format [[stage name, beginning time, end time],
        #[stage name....], ....]
        self.path = []
        self.transact_type = transact_type
    
    def __str__(self):
        return "Transact"

        
class Source:
    intensity = 0
    out_deals = 0
    proc = 0
    env = 0
    transact_count = 0
    generated = 0
    def __init__(self, env, intensity, name = "Source", \
    transact_type = "all", transact_count = 0):
        global sink
        self.intensity = intensity
        #format of out_deal [object , type of transact]
        self.out_deals = dict()
        self.env = env
        self.name = name
        self.process = env.process(self.production())
        self.sink = sink
        #type of transacts which will be produced by this source
        self.transact_type = transact_type
        #capacity - how many transacts could be produced
        self.capacity = transact_count
        #how many transacts should be produced in current period of activivty
        self.transact_count = transact_count
        self.generated = 0
    
    def __str__(self):
        return "Source"

        
    def production(self):
        global RUNTIME
        rand = 0
        counter = 0
        tr1 = 0
        #this is a path statistic record for this stage
        tmp_path_stage = []
        while True:
            try:
                if rand != 0:
                    yield self.env.timeout(rand)
                    #new transact
                #TODO: something wrong here
                elif rand == 0 and len(tmp_path_stage) == 0:
                    #adding information of this stage^
                    #name of stage, start and end times of presence
                    tmp_path_stage.append(self.name)
                    tmp_path_stage.append(self.env.now)
                    start = self.env.now
                    while (rand == 0):
                        producing_time = self.intensity()
                        rand = producing_time
                        
                    yield self.env.timeout(rand)
                
                rand = 0
                if self.generated < self.transact_count and self.transact_count > 0:
                    self.generated += 1
                    counter += 1
                    tr1 = Transact(("Transact "+str(counter)), transact_type = \
                    self.transact_type)   
                    #new transact generated only here
                    tmp_path_stage.append(self.env.now)
                    tr1.path.append(tmp_path_stage)
                    tmp_path_stage = []
                                
                #Searching for  ready outputs, send to first ready with 
                #appropriate transact_type preference
                    for o in self.out_deals.keys():
                        if  "all" in self.out_deals[o] or \
                            tr1.transact_type in\
                            self.out_deals[o]:
                            o.process.interrupt(["transact", tr1])
                            tr1 = 0
                            break
                    
                #If there is no ready output - renege
                    if tr1 != 0:
                        self.sink.process.interrupt(["transact", tr1])
                    
                elif self.generated == self.transact_count and \
                                                self.transact_count > 0:
                    yield self.env.timeout(100)
                    
                
            except simpy.Interrupt as i:
                if rand != 0:
                    rand = producing_time + start - self.env.now
                
                if i.cause[0] == "status":
                    self.out_deals[i.cause[1]] = i.cause[2]

                elif i.cause[0] =="signal":
                    if i.cause[1] == "open":
                        self.transact_count += self.capacity
                        
                    
                
        
    def add_outdeal(self, od):
        self.out_deals[od] = od.transact_type

            
            
class Channel:
    def __init__(self, env, processing_time, name = "Channel",\
    transact_type = {"all"}):
        global sink
        self.env = env
        self.process = env.process(self.processing())
        self.time_generator = processing_time
        self.processing_time = self.time_generator()
        self.remaining_time = 0
        self.status = "ready"
        self.event_receiver_signal_dict = dict()
        self.in_deals = set()
        #format of out_deal [object , type of transact]
        self.out_deals = dict()
        self.current_transact = 0
        self.name = name
        self.sink = sink
        self.transact_type = transact_type
    
    def __str__(self):
        return "Channel"
    
    def processing(self):
        tmp_path_stage = []
        while True:
            try:
                if self.status != "busy":
                    yield self.env.timeout(100) #magic number 100...like a nop
                else:
                    yield self.env.timeout(self.remaining_time)
                    #end processing time for this channel. Add it to path 
                    #of transact
                    tmp_path_stage.append(self.env.now)
                    self.current_transact.path.append(tmp_path_stage)
                    tmp_path_stage = []
                    self.status = "ready"
                    self.send_singnals()
                    for o in self.out_deals.keys():
                        if  "all" in self.out_deals[o] or \
                        self.current_transact.transact_type in\
                        self.out_deals[o]:
                            o.process.interrupt(["transact",\
                                self.current_transact])
                            self.current_transact = 0
                            break
                    
                    #if there is no way for current transact
                    if self.current_transact != 0:
                        self.sink.process.interrupt(["transact", \
                            self.current_transact])
                        
                    #busy, can't handle any type of transacts
                    for i in self.in_deals:
                        i.process.interrupt(["status", self, self.transact_type])

            except simpy.Interrupt as i:
                if i.cause[0] == "status":
                    self.out_deals[i.cause[1]]=i.cause[2]
                elif i.cause[0] == "transact":#transact arrived
                    if self.status == "busy":
                        self.remaining_time = self.start + self.processing_time \
                        - self.env.now
                        self.sink.process.interrupt(["transact",i.cause[1]])
                    else:
                        self.status = "busy"
                        self.send_singnals()
                        self.start = self.env.now
                        #start of transact processing
                        #add name and start time to path of transact statistic
                        tmp_path_stage.append(self.name)
                        tmp_path_stage.append(self.start)
                        self.processing_time = self.time_generator()
                        self.remaining_time = self.processing_time
                        self.current_transact = i.cause[1]
                        for ii in self.in_deals:
                            ii.process.interrupt(["status", self, set([])])                 
                        
    def send_singnals(self):
        if self.status in self.event_receiver_signal_dict.keys():
            for receiver in self.event_receiver_signal_dict[self.status].keys():
                receiver.process.interrupt(["signal", \
                        self.event_receiver_signal_dict[self.status][receiver]])
                    
                    
    def add_indeal(self, ii):
        self.in_deals.add(ii)
        
    def add_outdeal(self, od):
        self.out_deals[od] = od.transact_type
    
    def add_event_receiver_signal(self, event_receiver_signal_dict):
        self.event_receiver_signal_dict = event_receiver_signal_dict

        
class Buffer:
    def __init__(self, env, capacity, name = "Buffer", transact_type = \
    {"all"}):
        global sink
        self.env = env
        self.process = env.process(self.buffering())
        #self.status = "ready" #not using now
        self.in_deals = set()
        self.out_deals = dict()
        self.transact_queue = []
        self.status = "not full"        
        self.event_receiver_signal_dict = dict()
        self.capacity = capacity
        self.name = name
        self.sink = sink
        self.transact_type = transact_type
        #statistic : [time of changing] [new load]
        self.time_statistic = []
        self.load_statistic = []
    
    def __str__(self):
        return "Buffer"
        
        
    def buffering(self):
        tmp_path_stage = []
        self.time_statistic.append(self.env.now)
        self.load_statistic.append(len(self.transact_queue))
        while True:
            try:
                #simple idle loop, nop analogue (no operation)
                yield self.env.timeout(100)
            except simpy.Interrupt as i:
                #print(str(i.cause[0]))
                #someone is ready among outlines
                if i.cause[0] == "status" and len(i.cause[2]) != 0:
                    # if queue is empty change the status of interrupt thrower
                    #else change his status and transfer transact to him
                    #by interrupt
                    if len(self.transact_queue) == 0:       
                        self.out_deals[i.cause[1]] = i.cause[2]
                    else:
                        tmp_transact = self.transact_queue.pop(0)
                        self.status = "not full"
                        #send signals to receivers which are waiting for "not full"
                        self.send_singnals()
                        #print("Pop. Queue size = ", len(self.transact_queue))
                        #transact is now out of buffer. Adding to path instance
                        tmp_transact.path[len(tmp_transact.path)-1].\
                                                append(self.env.now)
                        self.out_deals[i.cause[1]] = i.cause[2]
                        i.cause[1].process.interrupt(["transact", tmp_transact])
                        for i in self.in_deals:
                            i.process.interrupt(["status" ,self, \
                                                self.transact_type])
                            
                #someone is busy among outlines            
                elif i.cause[0] == "status" and len(i.cause[2]) == 0:
                    self.out_deals[i.cause[1]] = i.cause[2]
                    
                #transact arrived
                elif i.cause[0] == "transact": 
                    tmp_transact = i.cause[1]
                    #print(len(self.transact_queue))
                    #Adding to path instance name of stage and time of appearence
                    tmp_path_stage.append(self.name)
                    tmp_path_stage.append(self.env.now)
                    #print("Transact appears in the buffer")
                    if len(self.transact_queue) == 0: 
                        for o in self.out_deals.keys():
                            if  "all" in self.out_deals[o] or \
                                tmp_transact.transact_type in\
                                self.out_deals[o]:
                                #transact is out of buffer. Adding to path instance
                                #time of arrival
                                tmp_path_stage.append(self.env.now)
                                tmp_transact.path.append(tmp_path_stage)
                                tmp_path_stage = []
                                o.process.interrupt(["transact",tmp_transact])
                                tmp_transact = 0
                                break
                        #if there are no ready outputs, place it in the queue
                        if tmp_transact != 0:
                            tmp_transact.path.append(tmp_path_stage)
                            self.transact_queue.append(tmp_transact)
                            tmp_path_stage = []
                            if len(self.transact_queue) == self.capacity:
                                self.status == "full"
                                self.send_singnals()
                            #print("Push. Queue size = ", len(self.transact_queue))
                            #print("Tmp_path",tmp_path_stage)
                            
                    #if there is a place in the queue   and it's not empty              
                    elif len(self.transact_queue) < self.capacity:
                        tmp_transact.path.append(tmp_path_stage)
                        self.transact_queue.append(tmp_transact)
                        tmp_path_stage = []
                        if len(self.transact_queue) == self.capacity:
                            self.status == "full"
                            self.send_singnals()
                        #print("Push. Queue size = ", len(self.transact_queue))
                        #print("Tmp_path",tmp_path_stage)
                        
                    #Reneged. Add to path instance end time
                    else:
                        tmp_path_stage.append(self.env.now)
                        tmp_transact.path.append(tmp_path_stage)
                        tmp_path_stage = []
                        print("Something wrong. queue is full. Reneged. Size="\
                        ,len(self.transact_queue))
                    
                    #at last we check the queue load
                    if len(self.transact_queue) == self.capacity:
                        self.status = "full"
                        self.send_singnals()
                        for i in self.in_deals:
                            i.process.interrupt(["status", self, \
                                                set([])])
                                                
                #if interrupt occurs then append statistic information
                self.time_statistic.append(self.env.now)
                self.load_statistic.append(len(self.transact_queue))
    
    def send_singnals(self):
        if self.status in self.event_receiver_signal_dict.keys():
            for receiver in self.event_receiver_signal_dict[self.status].keys():
                receiver.process.interrupt(["signal", \
                        self.event_receiver_signal_dict[self.status][receiver]])                    
                    
    def add_indeal(self, ii):
        self.in_deals.add(ii)
        
    def add_outdeal(self, od):
        self.out_deals[od] = od.transact_type
        
    def add_event_receiver_signal(self, event_receiver_signal_dict):
        self.event_receiver_signal_dict = event_receiver_signal_dict
        
    def show_statistic(self):
        global FIGURE
        plt.figure(FIGURE)
        FIGURE += 1
        plt.plot(self.time_statistic, self.load_statistic, 'r')
        title = "Load of buffer '"+self.name+"'"
        plt.title(title)
        plt.ylabel("Load of buffer with transacts")
        plt.xlabel("Evolution time")
        plt.show()

        
class Guard:
    def __init__(self, env, name = "Guard", state = "opened"):
        global sink
        self.env = env
        self.process = env.process(self.guarding())
        #self.status = "ready" #not using now
        self.in_deals = set()
        self.out_deals = dict()
        self.name = name
        self.state = state
        #self.sink = sink
        self.transact_type = set([])
    
    def guarding(self):
        for o in self.out_deals.keys():
            self.transact_type |= self.out_deals[o]
            
        for i in self.in_deals:
            i.process.interrupt(["status", self, \
                           self.transact_type])
                           
        while True:
            try:
                yield self.env.timeout(100)
            except simpy.Interrupt as i:
                if i.cause[0] == "signal":
                    #if opened received send types which could be processed now
                    if i.cause[1] == "open":
                        self.state = "opened"
                        for o in self.out_deals.keys():
                            self.transact_type |= self.out_deals[o]
                            
                        for i in self.in_deals:
                            i.process.interrupt(["status", self, \
                                                self.transact_type])
                        #always need to refresh current transact type                                                
                        self.transact_type = set([])
                    #if closed received send empty list set(no one types could
                    #be processed now)
                    elif i.cause[1] == "close":
                        self.state = "closed"
                        for i in self.in_deals:
                            i.process.interrupt(["status", self, \
                                                set([])])
                
                elif i.cause[0] == "status":
                    self.out_deals[i.cause[1]]=i.cause[2]
                    if self.state == "opened":
                        for o in self.out_deals.keys():
                            self.transact_type |= self.out_deals[o]
                            
                        for i in self.in_deals:
                            i.process.interrupt(["status", self, \
                                                self.transact_type])
                                                
                        self.transact_type = set([])
                    #else nothing to do
                    
                elif i.cause[0] == "transact":
                    if self.state == "opened":
                        tmp_transact = i.cause[1]
                        for o in self.out_deals.keys():
                            if  "all" in self.out_deals[o] or \
                                    tmp_transact.transact_type in\
                                    self.out_deals[o]:
                                o.process.interrupt(["transact",tmp_transact])
                                tmp_transact = 0
                                break

                    
    def add_indeal(self, ii):
        self.in_deals.add(ii)
        
    def add_outdeal(self, od):
        self.out_deals[od] = od.transact_type   

        
class Sink:
    def __init__(self, env, name = ""):
        self.env = env
        self.process = env.process(self.sinking())
        self.log = dict()
        self.name = name
        self.transact_type = {"all"}
    
    def sinking(self):
        while True:
            try:
                # idle cycle
                yield self.env.timeout(100)
            except simpy.Interrupt as i:
                if i.cause[0] == "transact":
                    #print(i.cause[1].name, " of type ", i.cause[1].transact_type)
                    for p in i.cause[1].path:
                        #print("   ", p[0],"  " ,p[1] , "  ", p[2])
                        if p[0] in self.log:
                            if i.cause[1].transact_type in self.log[p[0]]:
                                self.log[p[0]][i.cause[1].transact_type]\
                                ["count"] += 1               
                                self.log[p[0]][i.cause[1].transact_type]\
                                ["begin of work"].append(p[1])
                                self.log[p[0]][i.cause[1].transact_type]\
                                ["end of work"].append(p[2])                 
                                self.log[p[0]][i.cause[1].transact_type]\
                                ["time in work"] += p[2] - p[1]
                            else:
                                tmp = dict()
                                tmp["count"] = 1
                                tmp["begin of work"] = [p[1]]
                                tmp["end of work"] = [p[2]]
                                tmp["time in work"] = p[2] - p[1]
                                self.log[p[0]][i.cause[1].transact_type] = tmp
                        else:
                            tmp_type_of_stage = dict()
                            tmp_type_of_transact = dict()
                            tmp_type_of_transact["count"] = 1
                            tmp_type_of_transact["begin of work"] = [p[1]]
                            tmp_type_of_transact["end of work"] = [p[2]]
                            tmp_type_of_transact["time in work"] = p[2] - p[1]
                            tmp_type_of_stage[i.cause[1].transact_type] =\
                                tmp_type_of_transact
                            self.log[p[0]] = tmp_type_of_stage
                        
                    #self.log.append(i.cause[1])
                    #print(len(self.log))
            
    def show_statistic(self, stage_name="", transact_type=""):
        global FIGURE
        if stage_name == "":
            for stage_it in self.log.keys():
                #print("Stage:", stage_it)
                #for tr_type_it in self.log[stage_it]:
                    """print("   Transact type: ", tr_type_it,"\n",\
                    "     count:", self.log[stage_it][tr_type_it]["count"],"\n",\
                    "     time in work:", \
                    self.log[stage_it][tr_type_it]["time in work"])"""
        
        elif (stage_name not in self.log.keys()):
            print("There is no statistic for ", stage_name, " for output: ", \
            self.name)
        
        elif ((transact_type != "") and (transact_type not in self.log[stage_name].\
        keys())) or len(self.log[stage_name].keys()) == 0:
            print("There is no required transacts statistic for ", stage_name,\
            "for output: ", self.name)
                            
        else:#TODO: need some check here   
            plt.figure(FIGURE)
            FIGURE += 1
            bins = []
            bins_count = 20
            for i in range(bins_count + 1):
                bins.append(RUNTIME//bins_count * i)
                
            if transact_type == "":
                full_load = []
                for tr_type_it in self.log[stage_name]:
                        full_load += \
                            (self.log[stage_name][tr_type_it]["begin of work"])
                            
                plt.hist(full_load, \
                    bins)
                title = "Load of " + stage_name + " with transacts for output: "\
                + self.name
            else:
                plt.hist(self.log[stage_name][transact_type]["begin of work"],\
                    bins)
                title = "Load of " + stage_name + " with \'" +transact_type +\
                    "\' transacts for output: " + self.name
                       
            plt.title(title)
            plt.ylabel("Count of transacts")
            plt.xlabel("Time of simulation")
            plt.grid(True)
            plt.show()
            
    def add_indeal(self, ind):
        return

 


def add_deal(frm, to):
    frm.add_outdeal(to)
    to.add_indeal(frm)            
                    
             
             
env = simpy.Environment()
#sink for reneged. Don't change its name
sink = Sink(env, name="reneged")

#t = StageBase()
#Simple example

"""
output_sink = Sink(env, "processed")
s = Source(env, lambda:5)
c = Channel(env, lambda:10)
b = Buffer(env, 3)
g = Guard(env)
add_deal(s, g)
add_deal(g, b)
add_deal(b, c)
add_deal(c, output_sink)
c.add_event_receiver_signal({"ready":{g : "open"}, "busy":{g : "close"}})
env.run(until=RUNTIME)
output_sink.show_statistic(stage_name="Channel")
#output_sink.show_statistic(stage_name="Buffer")
output_sink.show_statistic(stage_name="Source")
sink.show_statistic(stage_name="Source")
"""